import pytest

from controller import reverse
from src.linked_list import LinkedList


@pytest.mark.skip("Not needed")
def test_empty():
    assert LinkedList([]).print_as_list() == []


def test_my_list():
    linked = LinkedList([1])
    assert linked.root.value == 1
    assert linked.root.next is None
    assert linked.print_as_list() == [1]

    linked = LinkedList(['a', 'b'])
    assert linked.root.next.value == 'b'
    assert linked.root.next.value == 'b'
    assert linked.print_as_list() == ['a', 'b']

    linked = LinkedList(['b', 'a', 'd'])
    assert linked.root.next.next.value == 'd'
    assert linked.print_as_list() == ['b', 'a', 'd']


def test_equality():
    assert LinkedList(['a']) == LinkedList(['a'])
    assert LinkedList(['a', 'b']) == LinkedList(['a', 'b'])


def test_value_not_equal():
    assert LinkedList(['a']) != LinkedList(['b'])
    assert LinkedList(['a', 'b']) != LinkedList(['a'])


def test_reverse_single():
    linked = LinkedList(['a'])
    assert linked.reverse_list() == LinkedList(['a'])


def test_reverse_linked():
    linked = LinkedList(['a', 'b'])
    assert linked.reverse_list() == LinkedList(['b', 'a'])


def test_reverse():
    assert reverse(['a']) == ['a']
    assert reverse(['a', 'b']) == ['b', 'a']
