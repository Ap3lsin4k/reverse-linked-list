from typing import Sequence

from src.node import Node


class LinkedList:
    def __init__(self, arr: Sequence):
        self.head = self.root = Node(arr[0]) if arr else None
        self._length = 1

        for i in range(1, len(arr)):
            self.head = self.insert(self.head, Node(arr[i]))

        self._length = len(arr)

    def __iter__(self):
        self.head = iter(self.root)
        return self

    def __next__(self):
        return next(self.head)

    def __repr__(self):
        return f"{self._length}: {self.root} @ {self.head}"

    def __eq__(self, other: 'LinkedList'):
        def eq_values():
            for a, b in zip(self.root, other):
                yield a.value == b.value
        return all(eq_values()) and len(self) == len(other)

    def insert(self, head, item):
        head.next = item
        self._length += 1
        return head.next

    def reverse_list(self):
        linked = LinkedList([])
        for tail in self:
            tail: Node = Node(tail.value)
            linked.insert(tail, linked.root)
            linked.root = tail
        return linked

    def print_as_list(self):
        flattened = []
        for node in self:
            flattened.append(node.value)
        return flattened

    def __len__(self):
        return self._length
