from typing import Optional


class Node:
    def __init__(self, data):
        self.value = data
        self.next: Optional[Node] = None

    def __iter__(self):
        self.head = self
        return self

    def __next__(self):
        if not self.head:
            raise StopIteration

        head = self.head
        self.head = self.head.next
        return head

    def __repr__(self):
        return f"{self.value} -> {self.next}"
