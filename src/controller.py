from linked_list import LinkedList


def reverse(arr):
    linked = LinkedList(arr)
    reversed_linked = linked.reverse_list()
    return reversed_linked.print_as_list()
